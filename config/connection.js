const mongoose = require("mongoose");
const MONGO_URL = "mongodb://localhost:27017";
const connectDB = async () => {
  await mongoose.connect(MONGO_URL, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  });
  console.log("db connect");
};
module.exports = connectDB;
