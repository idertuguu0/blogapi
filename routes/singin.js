const express = require("express");
const router = express.Router();
const Singin = require("../models/singin");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
router.get("/", async (req, res) => {
  try {
    const posts = await Singin.find();
    res.json(posts.filter((post) => post.email === req.user.email));
  } catch (err) {
    res.json({ message: err });
  }
  res.status(200).json({
    message: "handling GET requaests to /singins",
  });
});

router.post("/", (req, res, next) => {
  Singin.find({ email: req.body.email })
    .exec()
    .then((singin) => {
      if (singin.length > 1) {
        return res.status(401).json({
          message: "user failed",
        });
      } else {
        bcrypt.hash(req.body.password, 5, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err,
            });
          } else {
            const singin = new Singin({
              _id: new mongoose.Types.ObjectId(),
              name: req.body.name,
              email: req.body.email,
              password: hash,
              phone: req.body.phone,
            });
            singin
              .save()
              .then((result) => {
                console.log(result);
                res.status(201).json({
                  message: "user created",
                });
              })
              .catch((err) => {
                console.log(err);
                res.status(500).json({
                  error: err,
                });
              });
          }
        });
      }
    });
});

router.post("/login", (req, res, next) => {
  Singin.find({ email: req.body.email })
    .exec()
    .then((singin) => {
      if (singin.length < 1) {
        return res.status(401).json({
          message: "user failed",
        });
      }
      bcrypt.compare(req.body.password, singin[0].password, (err, result) => {
        if (err) {
          return res.status(401).json({
            message: "pass taarsangui",
          });
        }
        if (result) {
          const token = jwt.sign(
            {
              name: singin[0].name,
              singinId: singin[0]._id,
            },
            process.env.JWT_KEY,
            {
              expiresIn: "1h",
            }
          );
          return res.status(200).json({
            message: "Amjiltain newterlee",
            token: token,
            name: singin[0].name,
            userId: singin[0]._id,
          });
        }
        res.status(401).json({
          message: "Auth failed",
        });
      });
    })

    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.get("/:singinId", (req, res, next) => {
  Singin.findById({ _id: req.params.postId })
    .exec()
    .then((doc) => {
      console.log("from database", doc);
      res.status(200).json(doc);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.patch("/:singinId", (req, res, next) => {
  res.status(200).json({
    message: "update singins!",
  });
});

router.delete("/:singinId", (req, res, next) => {
  Singin.remove({ _id: req.params.singinId })
    .exec()
    .then((result) => {
      res.status(200).json({
        message: "post DELETE",
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
  res.status(200).json({
    message: "Deleted singins!",
  });
});

module.exports = router;
